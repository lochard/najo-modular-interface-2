{
	"patcher" : 	{
		"fileversion" : 1,
		"rect" : [ 395.0, 536.0, 1075.0, 461.0 ],
		"bglocked" : 0,
		"defrect" : [ 395.0, 536.0, 1075.0, 461.0 ],
		"openrect" : [ 0.0, 0.0, 0.0, 0.0 ],
		"openinpresentation" : 0,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 0,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 0,
		"toolbarvisible" : 1,
		"boxanimatetime" : 200,
		"imprint" : 0,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"boxes" : [ 			{
				"box" : 				{
					"maxclass" : "newobj",
					"text" : "prepend define_ls",
					"id" : "obj-1",
					"fontname" : "Arial",
					"numinlets" : 1,
					"numoutlets" : 1,
					"fontsize" : 12.0,
					"outlettype" : [ "" ],
					"patching_rect" : [ 92.0, 287.0, 106.0, 20.0 ]
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "nodes",
					"id" : "obj-20",
					"numinlets" : 1,
					"numoutlets" : 3,
					"xplace" : [ 0.083333, 0.166667, 0.25, 0.333333, 0.416667, 0.5, 0.583333, 0.666667 ],
					"outlettype" : [ "", "", "" ],
					"patching_rect" : [ 351.0, 199.0, 100.0, 100.0 ],
					"nodenumber" : 8,
					"yplace" : [ 0.083333, 0.166667, 0.25, 0.333333, 0.416667, 0.5, 0.583333, 0.666667 ],
					"nsize" : [ 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2 ]
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "flonum",
					"bgcolor" : [ 1.0, 1.0, 1.0, 0.0 ],
					"id" : "obj-11",
					"fontname" : "Arial",
					"numinlets" : 1,
					"presentation_rect" : [ 961.0, 50.0, 35.0, 18.0 ],
					"numoutlets" : 2,
					"fontface" : 1,
					"fontsize" : 10.0,
					"outlettype" : [ "float", "bang" ],
					"patching_rect" : [ 960.0, 45.0, 49.0, 18.0 ],
					"bordercolor" : [ 0.501961, 0.501961, 0.501961, 0.0 ],
					"presentation" : 1,
					"textcolor" : [ 1.0, 0.0, 0.0, 1.0 ],
					"triangle" : 0
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "comment",
					"text" : "Speaker 1",
					"presentation_linecount" : 2,
					"id" : "obj-12",
					"fontname" : "Arial Bold",
					"numinlets" : 1,
					"presentation_rect" : [ 901.0, 50.0, 39.0, 27.0 ],
					"numoutlets" : 0,
					"fontsize" : 9.0,
					"patching_rect" : [ 900.0, 45.0, 60.0, 17.0 ],
					"presentation" : 1
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "flonum",
					"bgcolor" : [ 1.0, 1.0, 1.0, 0.0 ],
					"id" : "obj-13",
					"fontname" : "Arial",
					"numinlets" : 1,
					"presentation_rect" : [ 841.0, 50.0, 35.0, 18.0 ],
					"numoutlets" : 2,
					"fontface" : 1,
					"fontsize" : 10.0,
					"outlettype" : [ "float", "bang" ],
					"patching_rect" : [ 840.0, 45.0, 49.0, 18.0 ],
					"bordercolor" : [ 0.501961, 0.501961, 0.501961, 0.0 ],
					"presentation" : 1,
					"textcolor" : [ 1.0, 0.0, 0.0, 1.0 ],
					"triangle" : 0
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "comment",
					"text" : "Speaker 1",
					"presentation_linecount" : 2,
					"id" : "obj-14",
					"fontname" : "Arial Bold",
					"numinlets" : 1,
					"presentation_rect" : [ 781.0, 50.0, 39.0, 27.0 ],
					"numoutlets" : 0,
					"fontsize" : 9.0,
					"patching_rect" : [ 780.0, 45.0, 60.0, 17.0 ],
					"presentation" : 1
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "flonum",
					"bgcolor" : [ 1.0, 1.0, 1.0, 0.0 ],
					"id" : "obj-15",
					"fontname" : "Arial",
					"numinlets" : 1,
					"presentation_rect" : [ 721.0, 50.0, 35.0, 18.0 ],
					"numoutlets" : 2,
					"fontface" : 1,
					"fontsize" : 10.0,
					"outlettype" : [ "float", "bang" ],
					"patching_rect" : [ 720.0, 45.0, 49.0, 18.0 ],
					"bordercolor" : [ 0.501961, 0.501961, 0.501961, 0.0 ],
					"presentation" : 1,
					"textcolor" : [ 1.0, 0.0, 0.0, 1.0 ],
					"triangle" : 0
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "comment",
					"text" : "Speaker 1",
					"presentation_linecount" : 2,
					"id" : "obj-16",
					"fontname" : "Arial Bold",
					"numinlets" : 1,
					"presentation_rect" : [ 661.0, 50.0, 39.0, 27.0 ],
					"numoutlets" : 0,
					"fontsize" : 9.0,
					"patching_rect" : [ 660.0, 45.0, 60.0, 17.0 ],
					"presentation" : 1
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "flonum",
					"bgcolor" : [ 1.0, 1.0, 1.0, 0.0 ],
					"id" : "obj-18",
					"fontname" : "Arial",
					"numinlets" : 1,
					"presentation_rect" : [ 601.0, 50.0, 35.0, 18.0 ],
					"numoutlets" : 2,
					"fontface" : 1,
					"fontsize" : 10.0,
					"outlettype" : [ "float", "bang" ],
					"patching_rect" : [ 600.0, 45.0, 49.0, 18.0 ],
					"bordercolor" : [ 0.501961, 0.501961, 0.501961, 0.0 ],
					"presentation" : 1,
					"textcolor" : [ 1.0, 0.0, 0.0, 1.0 ],
					"triangle" : 0
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "comment",
					"text" : "Speaker 1",
					"presentation_linecount" : 2,
					"id" : "obj-19",
					"fontname" : "Arial Bold",
					"numinlets" : 1,
					"presentation_rect" : [ 541.0, 50.0, 39.0, 27.0 ],
					"numoutlets" : 0,
					"fontsize" : 9.0,
					"patching_rect" : [ 540.0, 45.0, 60.0, 17.0 ],
					"presentation" : 1
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "flonum",
					"bgcolor" : [ 1.0, 1.0, 1.0, 0.0 ],
					"id" : "obj-7",
					"fontname" : "Arial",
					"numinlets" : 1,
					"presentation_rect" : [ 484.0, 48.0, 35.0, 18.0 ],
					"numoutlets" : 2,
					"fontface" : 1,
					"fontsize" : 10.0,
					"outlettype" : [ "float", "bang" ],
					"patching_rect" : [ 480.0, 45.0, 49.0, 18.0 ],
					"bordercolor" : [ 0.501961, 0.501961, 0.501961, 0.0 ],
					"presentation" : 1,
					"textcolor" : [ 1.0, 0.0, 0.0, 1.0 ],
					"triangle" : 0
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "comment",
					"text" : "Speaker 1",
					"presentation_linecount" : 2,
					"id" : "obj-8",
					"fontname" : "Arial Bold",
					"numinlets" : 1,
					"presentation_rect" : [ 424.0, 48.0, 39.0, 27.0 ],
					"numoutlets" : 0,
					"fontsize" : 9.0,
					"patching_rect" : [ 420.0, 45.0, 60.0, 17.0 ],
					"presentation" : 1
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "flonum",
					"bgcolor" : [ 1.0, 1.0, 1.0, 0.0 ],
					"id" : "obj-5",
					"fontname" : "Arial",
					"numinlets" : 1,
					"presentation_rect" : [ 367.0, 48.0, 35.0, 18.0 ],
					"numoutlets" : 2,
					"fontface" : 1,
					"fontsize" : 10.0,
					"outlettype" : [ "float", "bang" ],
					"patching_rect" : [ 360.0, 45.0, 49.0, 18.0 ],
					"bordercolor" : [ 0.501961, 0.501961, 0.501961, 0.0 ],
					"presentation" : 1,
					"textcolor" : [ 1.0, 0.0, 0.0, 1.0 ],
					"triangle" : 0
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "comment",
					"text" : "Speaker 1",
					"presentation_linecount" : 2,
					"id" : "obj-6",
					"fontname" : "Arial Bold",
					"numinlets" : 1,
					"presentation_rect" : [ 307.0, 48.0, 39.0, 27.0 ],
					"numoutlets" : 0,
					"fontsize" : 9.0,
					"patching_rect" : [ 300.0, 45.0, 60.0, 17.0 ],
					"presentation" : 1
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "flonum",
					"bgcolor" : [ 1.0, 1.0, 1.0, 0.0 ],
					"id" : "obj-3",
					"fontname" : "Arial",
					"numinlets" : 1,
					"presentation_rect" : [ 243.0, 42.0, 35.0, 18.0 ],
					"numoutlets" : 2,
					"fontface" : 1,
					"fontsize" : 10.0,
					"outlettype" : [ "float", "bang" ],
					"patching_rect" : [ 240.0, 45.0, 49.0, 18.0 ],
					"bordercolor" : [ 0.501961, 0.501961, 0.501961, 0.0 ],
					"presentation" : 1,
					"textcolor" : [ 1.0, 0.0, 0.0, 1.0 ],
					"triangle" : 0
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "comment",
					"text" : "Speaker 1",
					"presentation_linecount" : 2,
					"id" : "obj-4",
					"fontname" : "Arial Bold",
					"numinlets" : 1,
					"presentation_rect" : [ 183.0, 42.0, 39.0, 27.0 ],
					"numoutlets" : 0,
					"fontsize" : 9.0,
					"patching_rect" : [ 180.0, 45.0, 60.0, 17.0 ],
					"presentation" : 1
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "flonum",
					"bgcolor" : [ 1.0, 1.0, 1.0, 0.0 ],
					"id" : "obj-17",
					"fontname" : "Arial",
					"numinlets" : 1,
					"presentation_rect" : [ 28.0, 60.0, 35.0, 18.0 ],
					"numoutlets" : 2,
					"fontface" : 1,
					"fontsize" : 10.0,
					"outlettype" : [ "float", "bang" ],
					"patching_rect" : [ 120.0, 45.0, 49.0, 18.0 ],
					"bordercolor" : [ 0.501961, 0.501961, 0.501961, 0.0 ],
					"presentation" : 1,
					"textcolor" : [ 1.0, 0.0, 0.0, 1.0 ],
					"triangle" : 0
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "comment",
					"text" : "Speaker 1",
					"presentation_linecount" : 2,
					"id" : "obj-2",
					"fontname" : "Arial Bold",
					"numinlets" : 1,
					"presentation_rect" : [ 28.0, 45.0, 39.0, 27.0 ],
					"numoutlets" : 0,
					"fontsize" : 9.0,
					"patching_rect" : [ 60.0, 45.0, 60.0, 17.0 ],
					"presentation" : 1
				}

			}
 ],
		"lines" : [  ]
	}

}
