{
	"patcher" : 	{
		"fileversion" : 1,
		"rect" : [ 375.0, 236.0, 647.0, 509.0 ],
		"bgcolor" : [ 0.415686, 0.419608, 0.419608, 1.0 ],
		"bglocked" : 0,
		"defrect" : [ 375.0, 236.0, 647.0, 509.0 ],
		"openrect" : [ 0.0, 0.0, 0.0, 0.0 ],
		"openinpresentation" : 1,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 0,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 0,
		"toolbarvisible" : 1,
		"boxanimatetime" : 200,
		"imprint" : 0,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"boxes" : [ 			{
				"box" : 				{
					"maxclass" : "comment",
					"text" : "Presets",
					"presentation_rect" : [ 75.0, 1.0, 62.0, 18.0 ],
					"numinlets" : 1,
					"fontface" : 3,
					"fontsize" : 10.0,
					"patching_rect" : [ 230.0, 101.0, 60.0, 18.0 ],
					"numoutlets" : 0,
					"presentation" : 1,
					"id" : "obj-18",
					"fontname" : "Arial"
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "newobj",
					"text" : "r to_nmi_pattr",
					"numinlets" : 0,
					"fontsize" : 12.0,
					"patching_rect" : [ 232.0, 244.0, 85.0, 20.0 ],
					"numoutlets" : 1,
					"id" : "obj-17",
					"outlettype" : [ "" ],
					"fontname" : "Arial"
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "newobj",
					"text" : "loadmess 1 2 3 4 5 6 7 8 9",
					"numinlets" : 1,
					"fontsize" : 12.0,
					"patching_rect" : [ 448.0, 240.0, 152.0, 20.0 ],
					"numoutlets" : 1,
					"id" : "obj-23",
					"outlettype" : [ "" ],
					"fontname" : "Arial"
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "newobj",
					"text" : "vexpr $f1+$f2",
					"numinlets" : 2,
					"fontsize" : 12.0,
					"patching_rect" : [ 316.0, 270.0, 151.0, 20.0 ],
					"numoutlets" : 1,
					"id" : "obj-20",
					"outlettype" : [ "" ],
					"fontname" : "Arial"
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "nodes",
					"presentation_rect" : [ 210.0, 15.0, 135.0, 135.0 ],
					"numinlets" : 1,
					"knobsize" : 20.0,
					"patching_rect" : [ 316.0, 8.0, 189.0, 189.0 ],
					"numoutlets" : 3,
					"nodenumber" : 8,
					"presentation" : 1,
					"id" : "obj-16",
					"outlettype" : [ "", "", "" ],
					"displayknob" : 2,
					"candycane" : 8,
					"nsize" : [ 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.8 ],
					"yplace" : [ 0.079365, 0.873016, 0.100529, 0.910053, 0.230159, 0.039683, 0.544974, 0.746032 ],
					"xplace" : [ 0.063492, 0.068783, 0.666667, 0.407407, 0.904762, 0.230159, 0.132275, 0.793651 ]
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "newobj",
					"text" : "prepend recallmulti",
					"numinlets" : 1,
					"fontsize" : 12.0,
					"patching_rect" : [ 316.0, 299.082367, 112.0, 20.0 ],
					"numoutlets" : 1,
					"id" : "obj-15",
					"outlettype" : [ "" ],
					"fontname" : "Arial"
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "comment",
					"text" : "Windows",
					"presentation_rect" : [ 15.0, 105.0, 65.0, 20.0 ],
					"numinlets" : 1,
					"fontface" : 1,
					"fontsize" : 12.0,
					"patching_rect" : [ 114.0, 34.0, 66.0, 20.0 ],
					"numoutlets" : 0,
					"presentation" : 1,
					"id" : "obj-14",
					"fontname" : "Arial"
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "message",
					"text" : "storagewindow",
					"presentation_rect" : [ 15.0, 135.0, 86.0, 16.0 ],
					"numinlets" : 2,
					"fontface" : 1,
					"fontsize" : 10.0,
					"patching_rect" : [ 184.0, 64.0, 81.0, 16.0 ],
					"numoutlets" : 1,
					"presentation" : 1,
					"id" : "obj-21",
					"outlettype" : [ "" ],
					"fontname" : "Arial"
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "message",
					"text" : "clientwindow",
					"presentation_rect" : [ 15.0, 120.0, 88.0, 16.0 ],
					"numinlets" : 2,
					"fontface" : 1,
					"fontsize" : 10.0,
					"patching_rect" : [ 94.0, 64.0, 71.0, 16.0 ],
					"numoutlets" : 1,
					"presentation" : 1,
					"id" : "obj-22",
					"outlettype" : [ "" ],
					"fontname" : "Arial"
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "message",
					"text" : "read",
					"presentation_rect" : [ 150.0, 120.0, 45.0, 16.0 ],
					"numinlets" : 2,
					"fontface" : 1,
					"fontsize" : 10.0,
					"patching_rect" : [ 222.0, 199.0, 30.0, 16.0 ],
					"numoutlets" : 1,
					"presentation" : 1,
					"id" : "obj-43",
					"outlettype" : [ "" ],
					"fontname" : "Arial"
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "message",
					"text" : "write",
					"presentation_rect" : [ 150.0, 135.0, 45.0, 16.0 ],
					"numinlets" : 2,
					"fontface" : 1,
					"fontsize" : 10.0,
					"patching_rect" : [ 219.0, 163.0, 34.0, 16.0 ],
					"numoutlets" : 1,
					"presentation" : 1,
					"id" : "obj-44",
					"outlettype" : [ "" ],
					"fontname" : "Arial"
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "comment",
					"text" : "Bank",
					"presentation_rect" : [ 150.0, 105.0, 58.0, 20.0 ],
					"numinlets" : 1,
					"fontface" : 1,
					"fontsize" : 12.0,
					"patching_rect" : [ 217.0, 140.0, 54.0, 20.0 ],
					"numoutlets" : 0,
					"presentation" : 1,
					"id" : "obj-13",
					"fontname" : "Arial"
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "inlet",
					"numinlets" : 0,
					"patching_rect" : [ 262.0, 33.0, 25.0, 25.0 ],
					"numoutlets" : 1,
					"id" : "obj-10",
					"outlettype" : [ "" ],
					"comment" : ""
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "comment",
					"text" : "Store",
					"presentation_rect" : [ 15.0, 60.0, 58.0, 20.0 ],
					"numinlets" : 1,
					"fontface" : 1,
					"fontsize" : 12.0,
					"patching_rect" : [ 14.0, 176.0, 54.0, 20.0 ],
					"numoutlets" : 0,
					"presentation" : 1,
					"id" : "obj-12",
					"fontname" : "Arial"
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "comment",
					"text" : "Recall",
					"presentation_rect" : [ 15.0, 15.0, 58.0, 20.0 ],
					"numinlets" : 1,
					"fontface" : 1,
					"fontsize" : 12.0,
					"patching_rect" : [ 14.0, 100.0, 54.0, 20.0 ],
					"numoutlets" : 0,
					"presentation" : 1,
					"id" : "obj-11",
					"fontname" : "Arial"
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "message",
					"text" : "4",
					"presentation_rect" : [ 150.0, 30.0, 42.0, 16.0 ],
					"numinlets" : 2,
					"fontface" : 1,
					"fontsize" : 10.0,
					"patching_rect" : [ 147.0, 124.0, 31.0, 16.0 ],
					"numoutlets" : 1,
					"presentation" : 1,
					"id" : "obj-6",
					"outlettype" : [ "" ],
					"fontname" : "Arial"
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "message",
					"text" : "3",
					"presentation_rect" : [ 105.0, 30.0, 42.0, 16.0 ],
					"numinlets" : 2,
					"fontface" : 1,
					"fontsize" : 10.0,
					"patching_rect" : [ 102.0, 124.0, 31.0, 16.0 ],
					"numoutlets" : 1,
					"presentation" : 1,
					"id" : "obj-7",
					"outlettype" : [ "" ],
					"fontname" : "Arial"
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "message",
					"text" : "2",
					"presentation_rect" : [ 60.0, 30.0, 42.0, 16.0 ],
					"numinlets" : 2,
					"fontface" : 1,
					"fontsize" : 10.0,
					"patching_rect" : [ 57.0, 124.0, 31.0, 16.0 ],
					"numoutlets" : 1,
					"presentation" : 1,
					"id" : "obj-8",
					"outlettype" : [ "" ],
					"fontname" : "Arial"
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "message",
					"text" : "1",
					"presentation_rect" : [ 15.0, 30.0, 42.0, 16.0 ],
					"numinlets" : 2,
					"fontface" : 1,
					"fontsize" : 10.0,
					"patching_rect" : [ 12.0, 124.0, 31.0, 16.0 ],
					"numoutlets" : 1,
					"presentation" : 1,
					"id" : "obj-9",
					"outlettype" : [ "" ],
					"fontname" : "Arial"
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "message",
					"text" : "8",
					"presentation_rect" : [ 150.0, 45.0, 42.0, 16.0 ],
					"numinlets" : 2,
					"fontface" : 1,
					"fontsize" : 10.0,
					"patching_rect" : [ 147.0, 154.0, 31.0, 16.0 ],
					"numoutlets" : 1,
					"presentation" : 1,
					"id" : "obj-5",
					"outlettype" : [ "" ],
					"fontname" : "Arial"
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "message",
					"text" : "7",
					"presentation_rect" : [ 105.0, 45.0, 42.0, 16.0 ],
					"numinlets" : 2,
					"fontface" : 1,
					"fontsize" : 10.0,
					"patching_rect" : [ 102.0, 154.0, 31.0, 16.0 ],
					"numoutlets" : 1,
					"presentation" : 1,
					"id" : "obj-4",
					"outlettype" : [ "" ],
					"fontname" : "Arial"
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "message",
					"text" : "6",
					"presentation_rect" : [ 60.0, 45.0, 42.0, 16.0 ],
					"numinlets" : 2,
					"fontface" : 1,
					"fontsize" : 10.0,
					"patching_rect" : [ 57.0, 154.0, 31.0, 16.0 ],
					"numoutlets" : 1,
					"presentation" : 1,
					"id" : "obj-3",
					"outlettype" : [ "" ],
					"fontname" : "Arial"
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "message",
					"text" : "5",
					"presentation_rect" : [ 15.0, 45.0, 42.0, 16.0 ],
					"numinlets" : 2,
					"fontface" : 1,
					"fontsize" : 10.0,
					"patching_rect" : [ 10.0, 144.0, 31.0, 16.0 ],
					"numoutlets" : 1,
					"presentation" : 1,
					"id" : "obj-2",
					"outlettype" : [ "" ],
					"fontname" : "Arial"
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "outlet",
					"numinlets" : 1,
					"patching_rect" : [ 202.0, 356.0, 25.0, 25.0 ],
					"numoutlets" : 0,
					"id" : "obj-1",
					"comment" : ""
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "message",
					"text" : "store 8",
					"presentation_rect" : [ 150.0, 90.0, 43.0, 16.0 ],
					"numinlets" : 2,
					"fontface" : 1,
					"fontsize" : 10.0,
					"patching_rect" : [ 147.0, 229.0, 44.0, 16.0 ],
					"numoutlets" : 1,
					"presentation" : 1,
					"id" : "obj-29",
					"outlettype" : [ "" ],
					"fontname" : "Arial"
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "message",
					"text" : "store 7",
					"presentation_rect" : [ 105.0, 90.0, 43.0, 16.0 ],
					"numinlets" : 2,
					"fontface" : 1,
					"fontsize" : 10.0,
					"patching_rect" : [ 102.0, 229.0, 44.0, 16.0 ],
					"numoutlets" : 1,
					"presentation" : 1,
					"id" : "obj-30",
					"outlettype" : [ "" ],
					"fontname" : "Arial"
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "message",
					"text" : "store 6",
					"presentation_rect" : [ 60.0, 90.0, 43.0, 16.0 ],
					"numinlets" : 2,
					"fontface" : 1,
					"fontsize" : 10.0,
					"patching_rect" : [ 57.0, 229.0, 44.0, 16.0 ],
					"numoutlets" : 1,
					"presentation" : 1,
					"id" : "obj-26",
					"outlettype" : [ "" ],
					"fontname" : "Arial"
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "message",
					"text" : "store 5",
					"presentation_rect" : [ 15.0, 90.0, 43.0, 16.0 ],
					"numinlets" : 2,
					"fontface" : 1,
					"fontsize" : 10.0,
					"patching_rect" : [ 12.0, 229.0, 44.0, 16.0 ],
					"numoutlets" : 1,
					"presentation" : 1,
					"id" : "obj-27",
					"outlettype" : [ "" ],
					"fontname" : "Arial"
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "message",
					"text" : "store 4",
					"presentation_rect" : [ 150.0, 75.0, 45.0, 16.0 ],
					"numinlets" : 2,
					"fontface" : 1,
					"fontsize" : 10.0,
					"patching_rect" : [ 147.0, 199.0, 44.0, 16.0 ],
					"numoutlets" : 1,
					"presentation" : 1,
					"id" : "obj-28",
					"outlettype" : [ "" ],
					"fontname" : "Arial"
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "message",
					"text" : "store 3",
					"presentation_rect" : [ 105.0, 75.0, 43.0, 16.0 ],
					"numinlets" : 2,
					"fontface" : 1,
					"fontsize" : 10.0,
					"patching_rect" : [ 102.0, 199.0, 41.0, 16.0 ],
					"numoutlets" : 1,
					"presentation" : 1,
					"id" : "obj-33",
					"outlettype" : [ "" ],
					"fontname" : "Arial"
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "message",
					"text" : "store 2",
					"presentation_rect" : [ 60.0, 75.0, 43.0, 16.0 ],
					"numinlets" : 2,
					"fontface" : 1,
					"fontsize" : 10.0,
					"patching_rect" : [ 57.0, 199.0, 41.0, 16.0 ],
					"numoutlets" : 1,
					"presentation" : 1,
					"id" : "obj-39",
					"outlettype" : [ "" ],
					"fontname" : "Arial"
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "message",
					"text" : "store 1",
					"presentation_rect" : [ 15.0, 75.0, 43.0, 16.0 ],
					"numinlets" : 2,
					"fontface" : 1,
					"fontsize" : 10.0,
					"patching_rect" : [ 12.0, 199.0, 41.0, 16.0 ],
					"numoutlets" : 1,
					"presentation" : 1,
					"id" : "obj-40",
					"outlettype" : [ "" ],
					"fontname" : "Arial"
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"source" : [ "obj-5", 0 ],
					"destination" : [ "obj-1", 0 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-4", 0 ],
					"destination" : [ "obj-1", 0 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-3", 0 ],
					"destination" : [ "obj-1", 0 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-2", 0 ],
					"destination" : [ "obj-1", 0 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-6", 0 ],
					"destination" : [ "obj-1", 0 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-7", 0 ],
					"destination" : [ "obj-1", 0 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-8", 0 ],
					"destination" : [ "obj-1", 0 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-9", 0 ],
					"destination" : [ "obj-1", 0 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-28", 0 ],
					"destination" : [ "obj-1", 0 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-33", 0 ],
					"destination" : [ "obj-1", 0 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-39", 0 ],
					"destination" : [ "obj-1", 0 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-40", 0 ],
					"destination" : [ "obj-1", 0 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-29", 0 ],
					"destination" : [ "obj-1", 0 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-30", 0 ],
					"destination" : [ "obj-1", 0 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-26", 0 ],
					"destination" : [ "obj-1", 0 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-27", 0 ],
					"destination" : [ "obj-1", 0 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-44", 0 ],
					"destination" : [ "obj-1", 0 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-43", 0 ],
					"destination" : [ "obj-1", 0 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-23", 0 ],
					"destination" : [ "obj-20", 1 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-16", 0 ],
					"destination" : [ "obj-20", 0 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-20", 0 ],
					"destination" : [ "obj-15", 0 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-15", 0 ],
					"destination" : [ "obj-1", 0 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-17", 0 ],
					"destination" : [ "obj-1", 0 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-22", 0 ],
					"destination" : [ "obj-1", 0 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-21", 0 ],
					"destination" : [ "obj-1", 0 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
 ]
	}

}
