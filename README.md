## Najo Modular Interface v2.5 for Mac (Intel, Apple Silicon) & PC ##

© 2012-2022 Jean Lochard/Ircam

To install NMI, just copy the folder "NajoModularInterface_2" in your Max 8 Packages folder.

Please read "__NMI_Read_me_!.rtfd" for more inforamtion !
  
See changes in __NMI-Release_Notes.rtf
