{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 7,
			"minor" : 3,
			"revision" : 4,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"rect" : [ 204.0, 169.0, 1433.0, 864.0 ],
		"bglocked" : 0,
		"openinpresentation" : 0,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"globalpatchername" : "u682001185",
		"boxes" : [ 			{
				"box" : 				{
					"fontface" : 3,
					"id" : "obj-18",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1176.0, 631.0, 218.0, 20.0 ],
					"style" : "",
					"text" : "3) Active the timeline with Play"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 3,
					"id" : "obj-17",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1276.75, 368.5, 139.0, 20.0 ],
					"style" : "",
					"text" : "4) Explore presets !"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 3,
					"id" : "obj-16",
					"linecount" : 3,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 395.0, 345.5, 178.0, 47.0 ],
					"style" : "",
					"text" : "XSynth preset : switch this module on in order to hear the cross synthesis  --->"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 3,
					"id" : "obj-12",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ -1.0, 22.0, 104.0, 33.0 ],
					"style" : "",
					"text" : "2) Switch On this Module --->"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 3,
					"id" : "obj-2",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 597.0, 626.0, 124.0, 20.0 ],
					"style" : "",
					"text" : "1) Switch Audio On"
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-20",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-transport.maxpat",
					"numinlets" : 3,
					"numoutlets" : 0,
					"offset" : [ 0.0, 0.0 ],
					"patching_rect" : [ 1181.0, 659.0, 150.0, 165.0 ],
					"varname" : "transport-BP",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"hidden" : 1,
					"id" : "obj-60",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 554.0, 832.0, 69.0, 22.0 ],
					"save" : [ "#N", "thispatcher", ";", "#Q", "end", ";" ],
					"style" : "",
					"text" : "thispatcher"
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-54",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-master.maxpat",
					"numinlets" : 9,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 554.0, 659.0, 620.0, 165.0 ],
					"varname" : "master-BP",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-31",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-presets.maxpat",
					"numinlets" : 3,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 1185.5, 396.0, 241.5, 187.0 ],
					"varname" : "nmi-osc[17]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"autorestore" : "Effects.json",
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"hidden" : 1,
					"id" : "obj-19",
					"linecount" : 2,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1216.0, 589.0, 217.0, 35.0 ],
					"priority" : 					{
						"cenv1::PlayStop" : 1,
						"vst1::PlugFormat" : -2,
						"vst1::Plugin" : -1
					}
,
					"saved_object_attributes" : 					{
						"client_rect" : [ 4, 44, 358, 172 ],
						"parameter_enable" : 0,
						"storage_rect" : [ 232, 82, 1291, 816 ]
					}
,
					"style" : "",
					"text" : "pattrstorage Effects.json @savemode 2 @changemode 1 @autorestore 1",
					"varname" : "Effects.json"
				}

			}
, 			{
				"box" : 				{
					"args" : [ "track2" ],
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-36",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-track.maxpat",
					"numinlets" : 4,
					"numoutlets" : 0,
					"offset" : [ 0.0, 0.0 ],
					"patching_rect" : [ 259.0, 659.0, 90.0, 165.0 ],
					"varname" : "track2",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "track3" ],
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-40",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-track.maxpat",
					"numinlets" : 4,
					"numoutlets" : 0,
					"offset" : [ 0.0, 0.0 ],
					"patching_rect" : [ 453.0, 659.0, 90.0, 165.0 ],
					"varname" : "track3",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "track1" ],
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-8",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-track.maxpat",
					"numinlets" : 4,
					"numoutlets" : 0,
					"offset" : [ 0.0, 0.0 ],
					"patching_rect" : [ 94.0, 659.0, 90.0, 165.0 ],
					"varname" : "track1",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "groove1" ],
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-24",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-groove.maxpat",
					"numinlets" : 7,
					"numoutlets" : 2,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal", "int" ],
					"patching_rect" : [ 94.0, 35.0, 210.0, 90.0 ],
					"varname" : "groove1",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "delay1" ],
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-26",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-delay.maxpat",
					"numinlets" : 4,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 94.0, 151.0, 123.0, 61.0 ],
					"varname" : "delay1",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "freqshift1" ],
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-29",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-freqshift.maxpat",
					"numinlets" : 5,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 94.0, 230.0, 152.0, 61.0 ],
					"varname" : "freqshift1",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "psych1" ],
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-32",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-psych.maxpat",
					"numinlets" : 4,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 94.0, 312.5, 121.0, 64.0 ],
					"varname" : "psych1",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "disto1" ],
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-34",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-disto.maxpat",
					"numinlets" : 4,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 94.0, 396.0, 121.0, 64.0 ],
					"varname" : "disto1",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "degrade1" ],
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-37",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-degrade.maxpat",
					"numinlets" : 4,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 94.0, 480.0, 121.0, 64.0 ],
					"varname" : "degrade1",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "freeze1" ],
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-41",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-freeze.maxpat",
					"numinlets" : 4,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 259.0, 226.0, 121.0, 61.0 ],
					"varname" : "freeze1",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "clipping1" ],
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-43",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-clipping.maxpat",
					"numinlets" : 4,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 259.0, 305.0, 120.0, 61.0 ],
					"varname" : "clipping1",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "spectdelay" ],
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-45",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-spectdelay.maxpat",
					"numinlets" : 4,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 259.0, 383.5, 121.0, 150.5 ],
					"varname" : "spectdelay",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "supervp.trans1" ],
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-48",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-supervp.trans.maxpat",
					"numinlets" : 6,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 259.0, 550.0, 181.0, 88.0 ],
					"varname" : "supervp.trans1",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "xsynth1" ],
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-51",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-xsynth.maxpat",
					"numinlets" : 4,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 453.0, 488.5, 120.0, 150.0 ],
					"varname" : "xsynth1",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "groove2" ],
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-53",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-groove.maxpat",
					"numinlets" : 7,
					"numoutlets" : 2,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal", "int" ],
					"patching_rect" : [ 554.0, 372.5, 210.0, 90.0 ],
					"varname" : "groove2",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "sync1" ],
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-58",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-sync.maxpat",
					"numinlets" : 0,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "float" ],
					"patching_rect" : [ 437.0, 152.0, 30.0, 60.0 ],
					"varname" : "sync1",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "vst1" ],
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-3",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-vst.maxpat",
					"numinlets" : 7,
					"numoutlets" : 2,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 791.0, 345.5, 211.0, 121.0 ],
					"varname" : "vst1",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "track4" ],
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-6",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-track.maxpat",
					"numinlets" : 4,
					"numoutlets" : 0,
					"offset" : [ 0.0, 0.0 ],
					"patching_rect" : [ 791.0, 481.0, 90.0, 165.0 ],
					"varname" : "track4",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "filter1" ],
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-4",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-filter.maxpat",
					"numinlets" : 5,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1023.0, 273.5, 151.0, 91.0 ],
					"varname" : "filter1",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "moogfilter1" ],
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-11",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-moogfilter.maxpat",
					"numinlets" : 5,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1023.0, 375.5, 151.0, 91.0 ],
					"varname" : "moogfilter1",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "track5" ],
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-13",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-track.maxpat",
					"numinlets" : 4,
					"numoutlets" : 0,
					"offset" : [ 0.0, 0.0 ],
					"patching_rect" : [ 1023.0, 481.0, 90.0, 165.0 ],
					"varname" : "track5",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "lfo1" ],
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-15",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-lfo.maxpat",
					"numinlets" : 3,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 568.666748, 126.0, 90.0, 60.0 ],
					"varname" : "lfo1",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "ringmod1" ],
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-10",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-ringmod.maxpat",
					"numinlets" : 4,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 94.0, 562.5, 121.0, 61.5 ],
					"varname" : "ringmod1",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "reverb1" ],
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-14",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-reverb.maxpat",
					"numinlets" : 4,
					"numoutlets" : 2,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 791.0, 273.5, 180.0, 60.0 ],
					"varname" : "reverb1",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "delayAB1" ],
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-5",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-delayAB.maxpat",
					"numinlets" : 5,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 259.0, 151.0, 150.0, 60.0 ],
					"varname" : "delayAB1",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "cenv1" ],
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-22",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-cenv.maxpat",
					"numinlets" : 2,
					"numoutlets" : 2,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal", "float" ],
					"patching_rect" : [ 324.5, 58.5, 390.0, 60.0 ],
					"varname" : "cenv1",
					"viewvisibility" : 1
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-8", 0 ],
					"source" : [ "obj-10", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-13", 0 ],
					"source" : [ "obj-11", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"source" : [ "obj-14", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-11", 2 ],
					"order" : 1,
					"source" : [ "obj-15", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-48", 4 ],
					"order" : 0,
					"source" : [ "obj-15", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-31", 0 ],
					"hidden" : 1,
					"source" : [ "obj-19", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-5", 2 ],
					"source" : [ "obj-22", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-14", 0 ],
					"order" : 4,
					"source" : [ "obj-24", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-26", 0 ],
					"order" : 3,
					"source" : [ "obj-24", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-4", 0 ],
					"order" : 2,
					"source" : [ "obj-24", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-5", 0 ],
					"order" : 1,
					"source" : [ "obj-24", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-51", 0 ],
					"order" : 0,
					"source" : [ "obj-24", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-29", 0 ],
					"source" : [ "obj-26", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 0 ],
					"source" : [ "obj-29", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-6", 0 ],
					"source" : [ "obj-3", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 0 ],
					"hidden" : 1,
					"source" : [ "obj-31", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-34", 0 ],
					"source" : [ "obj-32", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-37", 0 ],
					"source" : [ "obj-34", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-10", 0 ],
					"source" : [ "obj-37", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-11", 0 ],
					"source" : [ "obj-4", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-43", 0 ],
					"source" : [ "obj-41", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-45", 0 ],
					"source" : [ "obj-43", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-48", 0 ],
					"source" : [ "obj-45", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-36", 0 ],
					"source" : [ "obj-48", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-41", 0 ],
					"source" : [ "obj-5", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-40", 0 ],
					"source" : [ "obj-51", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-51", 3 ],
					"source" : [ "obj-53", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-60", 0 ],
					"hidden" : 1,
					"source" : [ "obj-54", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-41", 3 ],
					"source" : [ "obj-58", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-54", 8 ],
					"hidden" : 1,
					"source" : [ "obj-60", 1 ]
				}

			}
 ],
		"parameters" : 		{
			"obj-14::obj-6::obj-52" : [ "live.dial[28]", "live.dial", 0 ],
			"obj-34::obj-6::obj-12" : [ "live.dial[8]", "live.dial", 0 ],
			"obj-32::obj-6::obj-12" : [ "live.dial[6]", "live.dial", 0 ],
			"obj-45::obj-21::obj-1" : [ "live.dial[15]", "live.dial", 0 ],
			"obj-29::obj-6::obj-1" : [ "live.dial[3]", "live.dial", 0 ],
			"obj-14::obj-6::obj-1" : [ "live.dial[27]", "live.dial", 0 ],
			"obj-43::obj-6::obj-1" : [ "live.dial[13]", "live.dial", 0 ],
			"obj-41::obj-6::obj-12" : [ "live.dial[12]", "live.dial", 0 ],
			"obj-10::obj-6::obj-1" : [ "live.dial[23]", "live.dial", 0 ],
			"obj-54::obj-2" : [ "Master", " ", 0 ],
			"obj-4::obj-6::obj-12" : [ "live.dial[20]", "live.dial", 0 ],
			"obj-48::obj-6::obj-12" : [ "live.dial[18]", "live.dial", 0 ],
			"obj-11::obj-6::obj-1" : [ "live.dial[21]", "live.dial", 0 ],
			"obj-26::obj-6::obj-12" : [ "live.dial[2]", "live.dial", 0 ],
			"obj-45::obj-21::obj-12" : [ "live.dial[16]", "live.dial", 0 ],
			"obj-37::obj-6::obj-1" : [ "live.dial[9]", "live.dial", 0 ],
			"obj-3::obj-6::obj-52" : [ "live.dial[30]", "live.dial", 0 ],
			"obj-34::obj-6::obj-1" : [ "live.dial[7]", "live.dial", 0 ],
			"obj-43::obj-6::obj-12" : [ "live.dial[14]", "live.dial", 0 ],
			"obj-32::obj-6::obj-1" : [ "live.dial[5]", "live.dial", 0 ],
			"obj-41::obj-6::obj-1" : [ "live.dial[11]", "live.dial", 0 ],
			"obj-10::obj-6::obj-12" : [ "live.dial[24]", "live.dial", 0 ],
			"obj-5::obj-6::obj-12" : [ "live.dial[26]", "live.dial", 0 ],
			"obj-3::obj-6::obj-1" : [ "live.dial[29]", "live.dial", 0 ],
			"obj-29::obj-6::obj-12" : [ "live.dial[4]", "live.dial", 0 ],
			"obj-26::obj-6::obj-1" : [ "live.dial[1]", "live.dial", 0 ],
			"obj-11::obj-6::obj-12" : [ "live.dial[22]", "live.dial", 0 ],
			"obj-5::obj-6::obj-1" : [ "live.dial[25]", "live.dial", 0 ],
			"obj-37::obj-6::obj-12" : [ "live.dial[10]", "live.dial", 0 ],
			"obj-4::obj-6::obj-1" : [ "live.dial[19]", "live.dial", 0 ],
			"obj-48::obj-6::obj-1" : [ "live.dial[17]", "live.dial", 0 ]
		}
,
		"dependency_cache" : [ 			{
				"name" : "nmi-cenv.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/misc/Control",
				"patcherrelativepath" : "../../misc/Control",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-paraboxctrl.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/patchers",
				"patcherrelativepath" : "../../patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-parabox.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/patchers",
				"patcherrelativepath" : "../../patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "ej.line.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/externals",
				"patcherrelativepath" : "../../externals",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-paraboxint.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/patchers",
				"patcherrelativepath" : "../../patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-delayAB.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/misc/Effects",
				"patcherrelativepath" : "../../misc/Effects",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-drywet.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/patchers",
				"patcherrelativepath" : "../../patchers",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "bypassmute3.jpg",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/media",
				"patcherrelativepath" : "../../media",
				"type" : "JPEG",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-reverb.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/misc/Effects",
				"patcherrelativepath" : "../../misc/Effects",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-rev4~.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/patchers",
				"patcherrelativepath" : "../../patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-drywetmonostereo.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/patchers",
				"patcherrelativepath" : "../../patchers",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-paraboxfreq.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/patchers",
				"patcherrelativepath" : "../../patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-ringmod.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/misc/Effects",
				"patcherrelativepath" : "../../misc/Effects",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-lfo.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/misc/Synthesis",
				"patcherrelativepath" : "../../misc/Synthesis",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-track.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/misc/Mix",
				"patcherrelativepath" : "../../misc/Mix",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-panpot.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/patchers",
				"patcherrelativepath" : "../../patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-paraboxgain.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/patchers",
				"patcherrelativepath" : "../../patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-moogfilter.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/misc/Effects",
				"patcherrelativepath" : "../../misc/Effects",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-filter.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/misc/Effects",
				"patcherrelativepath" : "../../misc/Effects",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-vst.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/misc/Effects",
				"patcherrelativepath" : "../../misc/Effects",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-drywetstereo.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/patchers",
				"patcherrelativepath" : "../../patchers",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-sync.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/misc/Control",
				"patcherrelativepath" : "../../misc/Control",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-groove.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/misc/Sound",
				"patcherrelativepath" : "../../misc/Sound",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-xsynth.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/misc/Effects",
				"patcherrelativepath" : "../../misc/Effects",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-xsynthfft~.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/patchers",
				"patcherrelativepath" : "../../patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-supervp.trans.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/misc/Effects",
				"patcherrelativepath" : "../../misc/Effects",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-spectdelay.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/misc/Effects",
				"patcherrelativepath" : "../../misc/Effects",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-spectdelayfft~.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/patchers",
				"patcherrelativepath" : "../../patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-bandtobin.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/patchers",
				"patcherrelativepath" : "../../patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-16thtonbwin.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/patchers",
				"patcherrelativepath" : "../../patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-clipping.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/misc/Effects",
				"patcherrelativepath" : "../../misc/Effects",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-clipingfft~.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/patchers",
				"patcherrelativepath" : "../../patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-freeze.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/misc/Effects",
				"patcherrelativepath" : "../../misc/Effects",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-freezefft~.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/patchers",
				"patcherrelativepath" : "../../patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-degrade.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/misc/Effects",
				"patcherrelativepath" : "../../misc/Effects",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-disto.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/misc/Effects",
				"patcherrelativepath" : "../../misc/Effects",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-psych.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/misc/Effects",
				"patcherrelativepath" : "../../misc/Effects",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-freqshift.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/misc/Effects",
				"patcherrelativepath" : "../../misc/Effects",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-delay.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/misc/Effects",
				"patcherrelativepath" : "../../misc/Effects",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "Effects.json",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/examples/Effects",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-presets.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/patchers",
				"patcherrelativepath" : "../../patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-master.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/patchers",
				"patcherrelativepath" : "../../patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-mem-pref.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/patchers",
				"patcherrelativepath" : "../../patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "preferences.txt",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/examples/Effects",
				"patcherrelativepath" : ".",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "modules.pat",
				"bootpath" : "~/Documents/Max 7/Packages/spat-3.4.1.1-forum/patchers",
				"patcherrelativepath" : "../../../spat-3.4.1.1-forum/patchers",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-objectsmenu.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/patchers",
				"patcherrelativepath" : "../../patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-objects-dim.coll",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/patchers",
				"patcherrelativepath" : "../../patchers",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-transport.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/patchers",
				"patcherrelativepath" : "../../patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "najo.multipan.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "supervp.trans~.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "psych~.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "shell.mxo",
				"type" : "iLaX"
			}
 ],
		"autosave" : 0
	}

}
