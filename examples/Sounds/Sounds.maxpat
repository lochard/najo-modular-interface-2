{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 7,
			"minor" : 3,
			"revision" : 4,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"rect" : [ 198.0, 130.0, 1448.0, 857.0 ],
		"bglocked" : 0,
		"openinpresentation" : 0,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"globalpatchername" : "u682001185",
		"boxes" : [ 			{
				"box" : 				{
					"fontface" : 3,
					"id" : "obj-23",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 627.5, 334.0, 311.0, 20.0 ],
					"style" : "",
					"text" : "Sampler preset : play midi notes from a keyboard"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 3,
					"id" : "obj-22",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1281.0, 373.0, 124.0, 20.0 ],
					"style" : "",
					"text" : "3) Explore presets"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 3,
					"id" : "obj-16",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1126.0, 637.0, 124.0, 20.0 ],
					"style" : "",
					"text" : "2) Push Play"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 3,
					"id" : "obj-7",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 476.0, 633.0, 124.0, 20.0 ],
					"style" : "",
					"text" : "1) Switch Audio On"
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-1",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-get_messages.maxpat",
					"numinlets" : 0,
					"numoutlets" : 0,
					"offset" : [ 0.0, 0.0 ],
					"patching_rect" : [ 1181.5, 119.5, 195.0, 157.0 ],
					"varname" : "nmi-osc[1]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-20",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-transport.maxpat",
					"numinlets" : 3,
					"numoutlets" : 0,
					"offset" : [ 0.0, 0.0 ],
					"patching_rect" : [ 1126.0, 659.0, 151.0, 171.0 ],
					"varname" : "transport-BP",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"hidden" : 1,
					"id" : "obj-60",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 495.0, 839.0, 69.0, 22.0 ],
					"save" : [ "#N", "thispatcher", ";", "#Q", "end", ";" ],
					"style" : "",
					"text" : "thispatcher"
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-54",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-master.maxpat",
					"numinlets" : 9,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 495.0, 659.0, 616.0, 171.0 ],
					"varname" : "master-BP",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-31",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-presets.maxpat",
					"numinlets" : 3,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 1181.5, 403.0, 240.0, 165.0 ],
					"varname" : "nmi-osc[17]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"autorestore" : "Sounds.json",
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"hidden" : 1,
					"id" : "obj-19",
					"linecount" : 2,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1213.5, 578.0, 218.0, 35.0 ],
					"priority" : 					{
						"cseq1::PlayStop" : 1,
						"gridseq1::PlayStop" : 1,
						"dtd::Fade" : -2,
						"dtd::Gain" : -1,
						"dtd::PlayStop" : 1,
						"supervp.scrub1::Sound" : 1,
						"loop2::Gain" : -1,
						"loop2::Sound" : 1,
						"loop2::Slices" : 1,
						"loop1::Gain" : -1,
						"loop1::Sound" : 1,
						"loop1::Slices" : 1,
						"ccenv1::PlayStop" : 1,
						"gran1::Sound" : 1
					}
,
					"saved_object_attributes" : 					{
						"client_rect" : [ 854, 172, 1208, 300 ],
						"parameter_enable" : 0,
						"storage_rect" : [ 766, 44, 1220, 302 ]
					}
,
					"style" : "",
					"text" : "pattrstorage Sounds.json @savemode 2 @changemode 1 @autorestore 1",
					"varname" : "Sounds.json"
				}

			}
, 			{
				"box" : 				{
					"args" : [ "track2" ],
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-36",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-track.maxpat",
					"numinlets" : 4,
					"numoutlets" : 0,
					"offset" : [ 0.0, 0.0 ],
					"patching_rect" : [ 135.0, 659.0, 90.0, 165.0 ],
					"varname" : "track2",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "track3" ],
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-40",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-track.maxpat",
					"numinlets" : 4,
					"numoutlets" : 0,
					"offset" : [ 0.0, 0.0 ],
					"patching_rect" : [ 241.0, 659.0, 90.0, 165.0 ],
					"varname" : "track3",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "track1" ],
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-8",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-track.maxpat",
					"numinlets" : 4,
					"numoutlets" : 0,
					"offset" : [ 0.0, 0.0 ],
					"patching_rect" : [ 30.0, 659.0, 90.0, 165.0 ],
					"varname" : "track1",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "gran1" ],
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-3",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-gran.maxpat",
					"numinlets" : 8,
					"numoutlets" : 2,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal", "" ],
					"patching_rect" : [ 14.0, 318.0, 240.0, 90.0 ],
					"varname" : "gran1",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "ccenv1" ],
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-6",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-cenv.maxpat",
					"numinlets" : 2,
					"numoutlets" : 2,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal", "float" ],
					"patching_rect" : [ 14.0, 15.0, 390.0, 60.0 ],
					"varname" : "ccenv1",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "loop1" ],
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-9",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-loop.maxpat",
					"numinlets" : 5,
					"numoutlets" : 2,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal", "int" ],
					"patching_rect" : [ 73.0, 467.0, 150.0, 122.0 ],
					"varname" : "loop1",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "loop2" ],
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-11",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-loop.maxpat",
					"numinlets" : 5,
					"numoutlets" : 2,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal", "int" ],
					"patching_rect" : [ 241.0, 467.0, 150.0, 122.0 ],
					"varname" : "loop2",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "supervp.scrub1" ],
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-13",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-supervp.scrub.maxpat",
					"numinlets" : 8,
					"numoutlets" : 2,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal", "int" ],
					"patching_rect" : [ 106.0, 142.0, 240.0, 120.0 ],
					"varname" : "supervp.scrub1",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "track4" ],
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-15",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-track.maxpat",
					"numinlets" : 4,
					"numoutlets" : 0,
					"offset" : [ 0.0, 0.0 ],
					"patching_rect" : [ 347.0, 660.0, 90.0, 165.0 ],
					"varname" : "track4",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "sampler1" ],
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-18",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-sampler.maxpat",
					"numinlets" : 12,
					"numoutlets" : 4,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal", "signal", "signal", "signal" ],
					"patching_rect" : [ 627.5, 358.0, 539.0, 211.0 ],
					"varname" : "sampler1",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "dtd" ],
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-4",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-dtd.maxpat",
					"numinlets" : 2,
					"numoutlets" : 8,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal" ],
					"patching_rect" : [ 469.5, 388.0, 120.0, 150.0 ],
					"varname" : "dtd",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "groove" ],
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-10",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-groove.maxpat",
					"numinlets" : 7,
					"numoutlets" : 2,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal", "int" ],
					"patching_rect" : [ 424.5, 218.0, 210.0, 90.0 ],
					"varname" : "groove",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "gridseq1" ],
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-14",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-gridseq.maxpat",
					"numinlets" : 2,
					"numoutlets" : 2,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal", "float" ],
					"patching_rect" : [ 495.0, 33.0, 180.0, 120.0 ],
					"varname" : "gridseq1",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "cseq1" ],
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-21",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-cseq.maxpat",
					"numinlets" : 2,
					"numoutlets" : 2,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal", "float" ],
					"patching_rect" : [ 735.0, 123.5, 360.0, 75.0 ],
					"varname" : "cseq1",
					"viewvisibility" : 1
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 0 ],
					"source" : [ "obj-10", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-40", 0 ],
					"source" : [ "obj-11", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-36", 0 ],
					"source" : [ "obj-13", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-10", 0 ],
					"source" : [ "obj-14", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-10", 3 ],
					"source" : [ "obj-14", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-54", 1 ],
					"source" : [ "obj-18", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-54", 0 ],
					"source" : [ "obj-18", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-31", 0 ],
					"hidden" : 1,
					"source" : [ "obj-19", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-10", 2 ],
					"source" : [ "obj-21", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-8", 0 ],
					"source" : [ "obj-3", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 0 ],
					"hidden" : 1,
					"source" : [ "obj-31", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-54", 1 ],
					"source" : [ "obj-4", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-54", 0 ],
					"source" : [ "obj-4", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-60", 0 ],
					"hidden" : 1,
					"source" : [ "obj-54", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-13", 0 ],
					"order" : 1,
					"source" : [ "obj-6", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"order" : 0,
					"source" : [ "obj-6", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-54", 8 ],
					"hidden" : 1,
					"source" : [ "obj-60", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-40", 0 ],
					"source" : [ "obj-9", 0 ]
				}

			}
 ],
		"parameters" : 		{
			"obj-21::obj-7" : [ "SyncRate-MN[1]", "SyncRate-MN", 0 ],
			"obj-14::obj-74" : [ "SyncMode-MN", "SyncMode-MN", 0 ],
			"obj-21::obj-74" : [ "SyncMode-MN[1]", "SyncMode-MN", 0 ],
			"obj-21::obj-26" : [ "rslider", "rslider", 0 ],
			"obj-21::obj-8" : [ "Values-MS", "Values-MS", 0 ],
			"obj-21::obj-80" : [ "Loop-TG[1]", "Loop-TG", 0 ],
			"obj-54::obj-2" : [ "Master", " ", 0 ],
			"obj-14::obj-8" : [ "live.grid", "live.grid", 0 ],
			"obj-14::obj-80" : [ "Loop-TG", "Loop-TG", 0 ],
			"obj-14::obj-7" : [ "SyncRate-MN", "SyncRate-MN", 0 ]
		}
,
		"dependency_cache" : [ 			{
				"name" : "nmi-cseq.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/misc/Control",
				"patcherrelativepath" : "../../misc/Control",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-paraboxint.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/patchers",
				"patcherrelativepath" : "../../patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-parabox.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/patchers",
				"patcherrelativepath" : "../../patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "ej.line.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/externals",
				"patcherrelativepath" : "../../externals",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-gridseq.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/misc/Control",
				"patcherrelativepath" : "../../misc/Control",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-groove.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/misc/Sound",
				"patcherrelativepath" : "../../misc/Sound",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-dtd.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/misc/Sound",
				"patcherrelativepath" : "../../misc/Sound",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-sampler.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/misc/Sound",
				"patcherrelativepath" : "../../misc/Sound",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-sampler-KG.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/patchers",
				"patcherrelativepath" : "../../patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-sampler-poly.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/patchers",
				"patcherrelativepath" : "../../patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-curve_ar.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/patchers",
				"patcherrelativepath" : "../../patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-track.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/misc/Mix",
				"patcherrelativepath" : "../../misc/Mix",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-panpot.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/patchers",
				"patcherrelativepath" : "../../patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-paraboxgain.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/patchers",
				"patcherrelativepath" : "../../patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-supervp.scrub.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/misc/Sound",
				"patcherrelativepath" : "../../misc/Sound",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-paraboxctrl.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/patchers",
				"patcherrelativepath" : "../../patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-loop.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/misc/Sound",
				"patcherrelativepath" : "../../misc/Sound",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-cenv.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/misc/Control",
				"patcherrelativepath" : "../../misc/Control",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-gran.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/misc/Sound",
				"patcherrelativepath" : "../../misc/Sound",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "Sounds.json",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/examples/Sounds",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-presets.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/patchers",
				"patcherrelativepath" : "../../patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-master.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/patchers",
				"patcherrelativepath" : "../../patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-mem-pref.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/patchers",
				"patcherrelativepath" : "../../patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "preferences.txt",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/examples/Sounds",
				"patcherrelativepath" : ".",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "modules.pat",
				"bootpath" : "~/Documents/Max 7/Packages/spat-3.4.1.1-forum/patchers",
				"patcherrelativepath" : "../../../spat-3.4.1.1-forum/patchers",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-objectsmenu.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/patchers",
				"patcherrelativepath" : "../../patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-objects-dim.coll",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/patchers",
				"patcherrelativepath" : "../../patchers",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-transport.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/patchers",
				"patcherrelativepath" : "../../patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-get_messages.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/patchers",
				"patcherrelativepath" : "../../patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "najo.multipan.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "supervp.scrub~.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "sogs~.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "shell.mxo",
				"type" : "iLaX"
			}
 ],
		"autosave" : 0
	}

}
