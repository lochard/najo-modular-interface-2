{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 7,
			"minor" : 3,
			"revision" : 4,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"rect" : [ 374.0, 375.0, 1278.0, 771.0 ],
		"bglocked" : 0,
		"openinpresentation" : 0,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"globalpatchername" : "u682001185",
		"boxes" : [ 			{
				"box" : 				{
					"fontface" : 3,
					"id" : "obj-35",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 708.0, 511.5, 171.0, 33.0 ],
					"style" : "",
					"text" : "Wave tables synthesis (play notes from a keybords)"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 3,
					"id" : "obj-25",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 111.0, 438.25, 124.0, 20.0 ],
					"style" : "",
					"text" : "Phase Modulation"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 3,
					"id" : "obj-23",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 12.0, 208.0, 124.0, 20.0 ],
					"style" : "",
					"text" : "Basic Oscillators"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 3,
					"id" : "obj-16",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 335.0, 15.0, 166.0, 33.0 ],
					"style" : "",
					"text" : "Mono Synth (play notes from a keybords)"
				}

			}
, 			{
				"box" : 				{
					"args" : [ "osc2" ],
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-15",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-osc.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 83.666672, 312.0, 90.0, 60.0 ],
					"varname" : "osc2[1]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"fontface" : 3,
					"id" : "obj-14",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 365.0, 538.0, 124.0, 20.0 ],
					"style" : "",
					"text" : "1) Switch Audio On"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 3,
					"id" : "obj-13",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1128.0, 243.0, 129.0, 20.0 ],
					"style" : "",
					"text" : "3) Explore presets"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 3,
					"id" : "obj-11",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1035.0, 546.0, 160.0, 20.0 ],
					"style" : "",
					"text" : "2) Switch Timeline On"
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-1",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-get_messages.maxpat",
					"numinlets" : 0,
					"numoutlets" : 0,
					"offset" : [ 0.0, 0.0 ],
					"patching_rect" : [ 994.0, 4.0, 195.0, 157.0 ],
					"varname" : "nmi-osc[1]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-20",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-transport.maxpat",
					"numinlets" : 3,
					"numoutlets" : 0,
					"offset" : [ 0.0, 0.0 ],
					"patching_rect" : [ 1074.0, 576.0, 150.0, 165.0 ],
					"varname" : "transport-BP",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"hidden" : 1,
					"id" : "obj-60",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 422.666687, 746.0, 107.0, 22.0 ],
					"save" : [ "#N", "thispatcher", ";", "#Q", "end", ";" ],
					"style" : "",
					"text" : "thispatcher"
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-54",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-master.maxpat",
					"numinlets" : 9,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 422.666687, 576.0, 615.0, 165.0 ],
					"varname" : "master-BP",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-31",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-presets.maxpat",
					"numinlets" : 3,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 978.0, 274.0, 241.0, 182.0 ],
					"varname" : "nmi-osc[17]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"autorestore" : "Synthesis.json",
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"hidden" : 1,
					"id" : "obj-19",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1023.0, 467.5, 236.0, 22.0 ],
					"priority" : 					{
						"env::PlayStop" : 1,
						"env::Dur" : 1,
						"notesseq1::PlayStop" : 1
					}
,
					"saved_object_attributes" : 					{
						"client_rect" : [ 4, 44, 358, 172 ],
						"parameter_enable" : 0,
						"storage_rect" : [ 232, 82, 817, 797 ]
					}
,
					"style" : "",
					"text" : "pattrstorage Synthesis.json @savemode 2",
					"varname" : "Synthesis.json"
				}

			}
, 			{
				"box" : 				{
					"args" : [ "track2" ],
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-36",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-track.maxpat",
					"numinlets" : 4,
					"numoutlets" : 0,
					"offset" : [ 0.0, 0.0 ],
					"patching_rect" : [ 127.0, 576.0, 90.0, 165.0 ],
					"varname" : "track2",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "track3" ],
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-40",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-track.maxpat",
					"numinlets" : 4,
					"numoutlets" : 0,
					"offset" : [ 0.0, 0.0 ],
					"patching_rect" : [ 224.0, 576.0, 90.0, 165.0 ],
					"varname" : "track3",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "track1" ],
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-8",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-track.maxpat",
					"numinlets" : 4,
					"numoutlets" : 0,
					"offset" : [ 0.0, 0.0 ],
					"patching_rect" : [ 32.0, 576.0, 90.0, 165.0 ],
					"varname" : "track1",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "sync1" ],
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-12",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-sync.maxpat",
					"numinlets" : 0,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "float" ],
					"patching_rect" : [ 132.666672, 0.5, 30.0, 60.0 ],
					"varname" : "sync1",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "ad1" ],
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-18",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-ad.maxpat",
					"numinlets" : 4,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 183.666672, 312.0, 120.0, 60.0 ],
					"varname" : "ad1",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "osc2" ],
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-22",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-osc.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 46.0, 239.5, 90.0, 60.0 ],
					"varname" : "osc2",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "phasemod1" ],
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-24",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-phasemod.maxpat",
					"numinlets" : 4,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 141.0, 467.5, 120.0, 75.0 ],
					"varname" : "phasemod1",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "supersaw1" ],
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-29",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-supersaw.maxpat",
					"numinlets" : 4,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 381.0, 239.5, 120.0, 75.0 ],
					"varname" : "supersaw1",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "adsr1" ],
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-32",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-adsr.maxpat",
					"numinlets" : 6,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 111.0, 73.5, 178.0, 60.0 ],
					"varname" : "adsr1",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "adsr2" ],
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-34",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-adsr.maxpat",
					"numinlets" : 6,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 154.666672, 160.5, 178.0, 60.0 ],
					"varname" : "adsr2",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "lfo2" ],
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-37",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-lfo.maxpat",
					"numinlets" : 3,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 613.0, 172.5, 90.0, 60.0 ],
					"varname" : "lfo2",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "notes1" ],
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-41",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-notes.maxpat",
					"numinlets" : 0,
					"numoutlets" : 3,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal", "signal", "int" ],
					"patching_rect" : [ 376.666687, 67.0, 90.0, 60.0 ],
					"varname" : "notes1",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "expadsr1" ],
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-28",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-expadsr.maxpat",
					"numinlets" : 6,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 414.666656, 164.0, 180.0, 60.0 ],
					"varname" : "expadsr1",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "expadsr2" ],
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-33",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-expadsr.maxpat",
					"numinlets" : 6,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 511.0, 254.5, 180.0, 60.0 ],
					"varname" : "expadsr2",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "notesseq1" ],
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-38",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-notesseq.maxpat",
					"numinlets" : 2,
					"numoutlets" : 2,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal", "float" ],
					"patching_rect" : [ 516.0, 52.0, 390.0, 75.0 ],
					"varname" : "notesseq1",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "wavetable1" ],
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-4",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-wavetable.maxpat",
					"numinlets" : 3,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 549.0, 498.0, 150.0, 60.0 ],
					"varname" : "wavetable1",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "expadsr3" ],
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-6",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-expadsr.maxpat",
					"numinlets" : 6,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 614.5, 350.5, 180.0, 60.0 ],
					"varname" : "expadsr3",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "env" ],
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-9",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-env.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 680.0, 423.0, 210.0, 60.0 ],
					"varname" : "env",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "filter1" ],
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-7",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-filter.maxpat",
					"numinlets" : 5,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 381.0, 335.5, 150.0, 90.0 ],
					"varname" : "filter1",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "lfo1" ],
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-21",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-lfo.maxpat",
					"numinlets" : 3,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 242.0, 387.0, 89.0, 56.5 ],
					"varname" : "lfo1",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "track4" ],
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-30",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-track.maxpat",
					"numinlets" : 4,
					"numoutlets" : 0,
					"offset" : [ 0.0, 0.0 ],
					"patching_rect" : [ 321.0, 576.0, 90.0, 165.0 ],
					"varname" : "track4",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "vsti" ],
					"bgmode" : 1,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-3",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "nmi-vsti.maxpat",
					"numinlets" : 6,
					"numoutlets" : 2,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 745.0, 188.0, 180.0, 120.0 ],
					"varname" : "vsti",
					"viewvisibility" : 1
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-18", 0 ],
					"order" : 2,
					"source" : [ "obj-12", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 0 ],
					"order" : 1,
					"source" : [ "obj-12", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-34", 0 ],
					"order" : 0,
					"source" : [ "obj-12", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-8", 0 ],
					"source" : [ "obj-15", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-24", 1 ],
					"source" : [ "obj-18", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-31", 0 ],
					"hidden" : 1,
					"source" : [ "obj-19", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-24", 3 ],
					"source" : [ "obj-21", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-8", 0 ],
					"source" : [ "obj-22", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-36", 0 ],
					"source" : [ "obj-24", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-29", 1 ],
					"source" : [ "obj-28", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-7", 0 ],
					"source" : [ "obj-29", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 0 ],
					"hidden" : 1,
					"source" : [ "obj-31", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-22", 1 ],
					"source" : [ "obj-32", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-7", 2 ],
					"source" : [ "obj-33", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 1 ],
					"source" : [ "obj-34", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-29", 0 ],
					"order" : 1,
					"source" : [ "obj-38", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-33", 0 ],
					"source" : [ "obj-38", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-4", 0 ],
					"order" : 0,
					"source" : [ "obj-38", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-30", 0 ],
					"source" : [ "obj-4", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-28", 0 ],
					"order" : 2,
					"source" : [ "obj-41", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-29", 0 ],
					"order" : 1,
					"source" : [ "obj-41", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-4", 0 ],
					"order" : 0,
					"source" : [ "obj-41", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-6", 0 ],
					"order" : 1,
					"source" : [ "obj-41", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 0 ],
					"order" : 0,
					"source" : [ "obj-41", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-60", 0 ],
					"hidden" : 1,
					"source" : [ "obj-54", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-4", 1 ],
					"source" : [ "obj-6", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-54", 8 ],
					"hidden" : 1,
					"source" : [ "obj-60", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-40", 0 ],
					"source" : [ "obj-7", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-4", 2 ],
					"source" : [ "obj-9", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-40", 0 ],
					"source" : [ "", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-40", 3 ],
					"source" : [ "", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-30", 0 ],
					"source" : [ "obj-3", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-40", 0 ],
					"source" : [ "obj-3", 0 ]
				}

			}
 ],
		"parameters" : 		{
			"obj-38::obj-26" : [ "rslider", "rslider", 0 ],
			"obj-38::obj-7" : [ "SyncRate-MN", "SyncRate-MN", 0 ],
			"obj-38::obj-61" : [ "live.grid", "live.grid", 0 ],
			"obj-38::obj-8" : [ "Values-MS", "Values-MS", 0 ],
			"obj-7::obj-6::obj-12" : [ "live.dial[2]", "live.dial", 0 ],
			"obj-54::obj-2" : [ "Master", " ", 0 ],
			"obj-38::obj-80" : [ "Loop-TG", "Loop-TG", 0 ],
			"obj-7::obj-6::obj-1" : [ "live.dial[1]", "live.dial", 0 ],
			"obj-38::obj-74" : [ "SyncMode-MN", "SyncMode-MN", 0 ]
		}
,
		"dependency_cache" : [ 			{
				"name" : "nmi-vsti.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/misc/Synthesis",
				"patcherrelativepath" : "../../misc/Synthesis",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-parabox.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/patchers",
				"patcherrelativepath" : "../../patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "DX7 V.maxsnap",
				"bootpath" : "~/Documents/Max 7/Snapshots",
				"patcherrelativepath" : "../../../../Snapshots",
				"type" : "mx@s",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-paraboxctrl.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/patchers",
				"patcherrelativepath" : "../../patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "ej.line.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/externals",
				"patcherrelativepath" : "../../externals",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-paraboxint.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/patchers",
				"patcherrelativepath" : "../../patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-track.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/misc/Mix",
				"patcherrelativepath" : "../../misc/Mix",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-panpot.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/patchers",
				"patcherrelativepath" : "../../patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-paraboxgain.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/patchers",
				"patcherrelativepath" : "../../patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-lfo.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/misc/Synthesis",
				"patcherrelativepath" : "../../misc/Synthesis",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-filter.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/misc/Effects",
				"patcherrelativepath" : "../../misc/Effects",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-paraboxfreq.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/patchers",
				"patcherrelativepath" : "../../patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-drywet.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/patchers",
				"patcherrelativepath" : "../../patchers",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "bypassmute3.jpg",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/media",
				"patcherrelativepath" : "../../media",
				"type" : "JPEG",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-env.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/misc/Synthesis",
				"patcherrelativepath" : "../../misc/Synthesis",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-expadsr.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/misc/Synthesis",
				"patcherrelativepath" : "../../misc/Synthesis",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-curve_adsr.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/patchers",
				"patcherrelativepath" : "../../patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-wavetable.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/misc/Synthesis",
				"patcherrelativepath" : "../../misc/Synthesis",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-notesseq.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/misc/Control",
				"patcherrelativepath" : "../../misc/Control",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-notes.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/misc/Control",
				"patcherrelativepath" : "../../misc/Control",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-adsr.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/misc/Synthesis",
				"patcherrelativepath" : "../../misc/Synthesis",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-supersaw.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/misc/Synthesis",
				"patcherrelativepath" : "../../misc/Synthesis",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-phasemod.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/misc/Synthesis",
				"patcherrelativepath" : "../../misc/Synthesis",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-osc.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/misc/Synthesis",
				"patcherrelativepath" : "../../misc/Synthesis",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-ad.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/misc/Synthesis",
				"patcherrelativepath" : "../../misc/Synthesis",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-sync.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/misc/Control",
				"patcherrelativepath" : "../../misc/Control",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "Synthesis.json",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/examples/Synthesis",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-presets.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/patchers",
				"patcherrelativepath" : "../../patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-master.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/patchers",
				"patcherrelativepath" : "../../patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-mem-pref.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/patchers",
				"patcherrelativepath" : "../../patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "preferences.txt",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/examples/Synthesis",
				"patcherrelativepath" : ".",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "modules.pat",
				"bootpath" : "~/Documents/Max 7/Packages/spat-3.4.1.1-forum/patchers",
				"patcherrelativepath" : "../../../spat-3.4.1.1-forum/patchers",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-objectsmenu.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/patchers",
				"patcherrelativepath" : "../../patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-objects-dim.coll",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/patchers",
				"patcherrelativepath" : "../../patchers",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-transport.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/patchers",
				"patcherrelativepath" : "../../patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "nmi-get_messages.maxpat",
				"bootpath" : "~/Documents/Max 7/Packages/NajoModularInterface_2/patchers",
				"patcherrelativepath" : "../../patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "najo.multipan.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "shell.mxo",
				"type" : "iLaX"
			}
 ],
		"autosave" : 0
	}

}
