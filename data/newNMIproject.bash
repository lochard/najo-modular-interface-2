#!/bin/bash

################################
# Creation Methodes
################################


function CreateProjectFolder {

		if [[ ! -e "$ProjectPath" ]] ; then
			mkdir "$ProjectPath";
			echo "."
		fi
		
}

function CreateSubFolder {
		cd "$ProjectPath"
		if [[ ! -e $FolderName ]] ; then
			mkdir $FolderName;
			echo "."
		fi
		cd $HOME
}



################################
# Creation process
################################

clear

PackagePath="$1"
ProjectPath="$2"
NewPatchName="$3"

echo "Creating New Project Folder."


# Folders And Files To Create in Project Folder
###############################################

NewFolders=(dtd wavetables sounds)
NewFiles=(Default.maxpat preferences.txt presets.json)


if [ "$ProjectPath" != "" ]; then

	
	# Create Project Folder
	##################################
	
	CreateProjectFolder

	# Create Project Subfolders
	##################################

	if [ "${NewFolders[0]}" != "" ]; then

		for i in "${NewFolders[@]}";

		do

   			FolderName=$i
			CreateSubFolder
			
		done
	fi
	
	# Copy other files in Project
	##################################
	
	if [ "${NewFiles[0]}" != "" ]; then
		for i in "${NewFiles[@]}";

		do

   			FileName="$i"
			cp "$PackagePath/examples/Default-Project/$FileName" "$ProjectPath"
			
		done
		
		cd "$ProjectPath"
		mv "Default.maxpat" "$NewPatchName.maxpat"
		open "$NewPatchName.maxpat"
		cd $HOME
		echo "."
		
		
	fi	
fi