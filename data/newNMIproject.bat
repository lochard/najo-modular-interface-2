echo off
rem # Creation d'un projet pour NMI
rem ###############################################

rem recuperation des variable à partir des options de la commande
set "SourcePath=%HOMEPATH%\Documents\Max 7\Packages\NajoModularInterface_2\examples\Default-Project\"
echo %SourcePath%
set "ProjectPath=%1"
echo %ProjectPath%
set "NewPatchName=%2"
echo %NewPatchName%

rem # Folders And Files To Create in Project Folder
rem ###############################################

set NewFolders=(dtd wavetable sounds)
set NewFiles=(Default.maxpat preferences.txt presets.json)

echo Creating New Project Folder

mkdir %ProjectPath%
cd %ProjectPath%
for %%i in %NewFolders% do mkdir %%i
	
for %%i in %NewFiles% do copy "%SourcePath%%%i" %ProjectPath%												
		
ren "Default.maxpat" %NewPatchName%.maxpat
%NewPatchName%.maxpat
cd %HOMEPATH%